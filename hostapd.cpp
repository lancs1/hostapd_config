/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2016-2020 the prplMesh contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#include <algorithm>
#include <fstream>

#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/stat.h>
#include <fcntl.h>
#include <sys/un.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>

#include "hostapd.h"
#include "os.h"

#define CONFIG_CTRL_IFACE_DIR "/var/run/hostapd/"

#ifndef CONFIG_CTRL_IFACE_CLIENT_DIR
#define CONFIG_CTRL_IFACE_CLIENT_DIR "/tmp"
#endif /* CONFIG_CTRL_IFACE_CLIENT_DIR */

#ifndef CONFIG_CTRL_IFACE_CLIENT_PREFIX
#define CONFIG_CTRL_IFACE_CLIENT_PREFIX "wpa_ctrl_"
#endif

/**
 * struct wpa_ctrl - Internal structure for control interface library
 *
 * This structure is used by the wpa_supplicant/hostapd control interface
 * library to store internal data. Programs using the library should not touch
 * this data directly. They can only use the pointer to the data structure as
 * an identifier for the control interface connection and use this as one of
 * the arguments for most of the control interface library functions.
 */
struct wpa_ctrl {
	int s;
	struct sockaddr_un local;
	struct sockaddr_un dest;
};

struct wpa_ctrl * wpa_ctrl_open(const char *ctrl_path, const char *cli_path)
{
	struct wpa_ctrl *ctrl;
	static int counter = 0;
	int ret;
	size_t res;
	int tries = 0;
	int flags;

	if (ctrl_path == NULL)
		return NULL;

	ctrl = (struct wpa_ctrl *)os_zalloc(sizeof(*ctrl));
	if (ctrl == NULL)
		return NULL;

	ctrl->s = socket(PF_UNIX, SOCK_DGRAM, 0);
	if (ctrl->s < 0) {
		free(ctrl);
		return NULL;
	}

	ctrl->local.sun_family = AF_UNIX;
	counter++;
try_again:
	if (cli_path && cli_path[0] == '/') {
		ret = snprintf(ctrl->local.sun_path,
				  sizeof(ctrl->local.sun_path),
				  "%s/" CONFIG_CTRL_IFACE_CLIENT_PREFIX "%d-%d",
				  cli_path, (int) getpid(), counter);
	} else {
		ret = snprintf(ctrl->local.sun_path,
				  sizeof(ctrl->local.sun_path),
				  CONFIG_CTRL_IFACE_CLIENT_DIR "/"
				  CONFIG_CTRL_IFACE_CLIENT_PREFIX "%d-%d",
				  (int) getpid(), counter);
	}
	if (os_snprintf_error(sizeof(ctrl->local.sun_path), ret)) {
		close(ctrl->s);
		free(ctrl);
		return NULL;
	}
	tries++;

	if (bind(ctrl->s, (struct sockaddr *) &ctrl->local,
		    sizeof(ctrl->local)) < 0) {
		if (errno == EADDRINUSE && tries < 2) {
			/*
			 * getpid() returns unique identifier for this instance
			 * of wpa_ctrl, so the existing socket file must have
			 * been left by unclean termination of an earlier run.
			 * Remove the file and try again.
			 */
			unlink(ctrl->local.sun_path);
			goto try_again;
		}
		close(ctrl->s);
		free(ctrl);
		return NULL;
	}

	ctrl->dest.sun_family = AF_UNIX;
	if (strncmp(ctrl_path, "@abstract:", 10) == 0) {
		ctrl->dest.sun_path[0] = '\0';
		strlcpy(ctrl->dest.sun_path + 1, ctrl_path + 10,
			   sizeof(ctrl->dest.sun_path) - 1);
	} else {
		res = strlcpy(ctrl->dest.sun_path, ctrl_path,
				 sizeof(ctrl->dest.sun_path));
		if (res >= sizeof(ctrl->dest.sun_path)) {
			close(ctrl->s);
			free(ctrl);
			return NULL;
		}
	}
	if (connect(ctrl->s, (struct sockaddr *) &ctrl->dest,
		    sizeof(ctrl->dest)) < 0) {
		close(ctrl->s);
		unlink(ctrl->local.sun_path);
		free(ctrl);
		return NULL;
	}

	/*
	 * Make socket non-blocking so that we don't hang forever if
	 * target dies unexpectedly.
	 */
	flags = fcntl(ctrl->s, F_GETFL);
	if (flags >= 0) {
		flags |= O_NONBLOCK;
		if (fcntl(ctrl->s, F_SETFL, flags) < 0) {
			perror("fcntl(ctrl->s, O_NONBLOCK)");
			/* Not fatal, continue on.*/
		}
	}

	return ctrl;
}


void wpa_ctrl_close(struct wpa_ctrl *ctrl)
{
	if (ctrl == NULL)
		return;
	unlink(ctrl->local.sun_path);
	if (ctrl->s >= 0)
		close(ctrl->s);
	free(ctrl);
}

int wpa_ctrl_request(struct wpa_ctrl *ctrl, const char *cmd, size_t cmd_len,
		     char *reply, size_t *reply_len,
		     void (*msg_cb)(char *msg, size_t len))
{
	struct timeval tv;
	struct os_reltime started_at;
	int res;
	fd_set rfds;
	const char *_cmd;
	char *cmd_buf = NULL;
	size_t _cmd_len;
	_cmd = cmd;
	_cmd_len = cmd_len;

	errno = 0;
	started_at.sec = 0;
	started_at.usec = 0;
retry_send:
	if (send(ctrl->s, _cmd, _cmd_len, 0) < 0) {
		if (errno == EAGAIN || errno == EBUSY || errno == EWOULDBLOCK)
		{
			/*
			 * Must be a non-blocking socket... Try for a bit
			 * longer before giving up.
			 */
			if (started_at.sec == 0)
				os_get_reltime(&started_at);
			else {
				struct os_reltime n;
				os_get_reltime(&n);
				/* Try for a few seconds. */
				if (os_reltime_expired(&n, &started_at, 5))
					goto send_err;
			}
			os_sleep(1, 0);
			goto retry_send;
		}
	send_err:
		os_free(cmd_buf);
		return -1;
	}
	os_free(cmd_buf);

	for (;;) {
		tv.tv_sec = 10;
		tv.tv_usec = 0;
		FD_ZERO(&rfds);
		FD_SET(ctrl->s, &rfds);
		res = select(ctrl->s + 1, &rfds, NULL, NULL, &tv);
		if (res < 0 && errno == EINTR)
			continue;
		if (res < 0)
			return res;
		if (FD_ISSET(ctrl->s, &rfds)) {
			res = recv(ctrl->s, reply, *reply_len, 0);
			if (res < 0)
				return res;
			if ((res > 0 && reply[0] == '<') ||
			    (res > 6 && strncmp(reply, "IFNAME=", 7) == 0)) {
				/* This is an unsolicited message from
				 * wpa_supplicant, not the reply to the
				 * request. Use msg_cb to report this to the
				 * caller. */
				if (msg_cb) {
					/* Make sure the message is nul
					 * terminated. */
					if ((size_t) res == *reply_len)
						res = (*reply_len) - 1;
					reply[res] = '\0';
					msg_cb(reply, res);
				}
				continue;
			}
			*reply_len = res;
			break;
		} else {
			return -2;
		}
	}
	return 0;
}

static int wpa_ctrl_command(struct wpa_ctrl *ctrl, const char *cmd, int print)
{
	char buf[4096];
	size_t len;
	int ret;

	if (ctrl == NULL) {
		printf("Not connected to hostapd - command dropped.\n");
		return -1;
	}
	len = sizeof(buf) - 1;
	ret = wpa_ctrl_request(ctrl, cmd, strlen(cmd), buf, &len,
			       NULL);
	if (ret == -2) {
		printf("'%s' command timed out.\n", cmd);
		return -2;
	} else if (ret < 0) {
		printf("'%s' command failed.\n", cmd);
		return -1;
	}
	if (print) {
		buf[len] = '\0';
		printf("%s", buf);
	}
	return 0;
}

static int hostapd_cli_cmd_update(struct wpa_ctrl *ctrl)
{
	char cmd[10];

	snprintf(cmd, sizeof(cmd), "UPDATE ");

	return wpa_ctrl_command(ctrl, cmd, 1);
}

#ifdef __cplusplus
}
#endif

class Configuration {

public:
    /**
     * @brief construct a Configurationn object
     * @param file_name the name of the configuration file.
     */
    explicit Configuration(const std::string &file_name);

    ~Configuration()                     = default;
    Configuration(const Configuration &) = default;
    Configuration(Configuration &&)      = default;

    /**
     * @brief for simple use in if statements
     * @return true or false with the following meaning:
     * true - configuration is non empty and valid
     * false - otherwise
     */
    operator bool() const;

    /**
     * @brief load the configuration
     * @details loads the file this object is constructed with
     * @param vap_indications - set the indications that a vap
     * configuration section begins. e.g. "bss=" and/or "interface="
     * note that the equal sign is expected to be part of vap_indication
     * std::string vap_indication("interface=");
     * vap indication can be any of the parameters supplied
     * @return *this (as bool, see above)
     */
    template <class... StringIndications> bool load(const StringIndications... vap_indications)
    {
        return load(std::set<std::string>{vap_indications...});
    }

    /**
     * @brief stores the configuration
     * @details stores the internal representation of the configuration
     * into the file it was loaded from, effectively changing the configuration
     * @return *this (as bool, see above)
     */
    bool store();

    /**
     * @brief set key/value in the head section
     * @details set the key/vale in the head section,
     * either replace or create.
     * @note comments in the head section are not supported
     * for example, if the key/value line was commented before
     * the call to this function, it would be uncommented afterwards
     * @param
     * - the key to set its value (string)
     * - the value (string)
     */
    bool set_create_head_value(const std::string &key, const std::string &value);

    /**
     * @brief set key/value in the head section
     * @details set the key/vale in the head section,
     * either replace or create.
     * @note comments in the head section are not supported
     * for example, if the key/value line was commented before
     * the call to this function, it would be uncommented afterwards
     * @param
     * - the key to set its value (string)
     * - the value (int)
     */
    bool set_create_head_value(const std::string &key, const int value);

    /**
     * @brief get the value of the given key from the head
     * @param
     * - the key to get its value (string)
     * @return a string with the value or empty if not found
     */
    std::string get_head_value(const std::string &key);

    /**
     * @brief set key/value for the given vap
     * @details set the key/vale for the given vap, either replace or create.
     * @param
     * - the vap to set the value for (string)
     * - the key to set its value (string)
     * - the value (string)
     * @return
     * true - the vap exists, values were set.
     * false - the given vap was not found
     */
    bool set_create_vap_value(const std::string &vap, const std::string &key,
                              const std::string &value);

    /**
     * @brief set key/value for the given vap
     * @details set the key/vale for the given vap, either replace or create.
     * @param
     * - the vap to set the value for (string)
     * - the key to set its value (string)
     * - the value (int)
     * @return
     * true - the vap exists, values were set.
     * false - the given vap was not found
     */
    bool set_create_vap_value(const std::string &vap, const std::string &key, const int value);

    /**
     * @brief get the value of the given key for the given vap
     * @param
     * - the vap to get the value for (string)
     * - the key to get its value (string)
     * @return a string with the value or empty if not found
     */
    std::string get_vap_value(const std::string &vap, const std::string &key);

    /**
     * @brief disables vap by setting start_disabled to 1 and removing
     * the "ssid" parameter.
     * @param vap The vap to disable.
     * @return true on success, false otherwise.
     */
    bool disable_vap(const std::string &vap);

    /**
     * @brief apply func to all ap vaps
     * @details apply func to all vaps that ap_predicate returns
     * true for them. this enables leaving STAs vaps untouched for example.
     * @param func has the following interface:         void f(const std::string &vap);
     * @param ap_predicate has the following interface: bool f(const std::string &vap);
     */
    template <class func, class ap_predicate> void for_all_ap_vaps(func, ap_predicate);

    /**
     * @brief apply func to all ap vaps, increment the given iterator
     * with each call to func: f(current_vap, ++user_itertor)
     * @details apply func to all vaps that ap_predicate returns true for
     * while incrementing the user iterator.
     * this functionality enables iterating over two containers
     * in paralel: the ap-vaps and the user container.
     * @param func has the following interface:         template<class iter> f(const std::string& vap, iter user_iterator)
     * @param iter - iterator to the begining of the user sequence
     * @param ap_predicate has the following interface: bool f(const std::string &vap);
     */
    template <class func, class iter, class ap_predicate>
    void for_all_ap_vaps(func, iter current, const iter end, ap_predicate);

    /**
     * @brief for debug: return the last internal message
     * @details each action on this class changes its internal
     * message (similar to errno) - for debug usage
     * @return string describing the last message
     */
    const std::string &get_last_message() const;

private:
    /**
     * @brief helper: load configuration based on array of strings
     * indicating vap separation.
     * @param array of separations
     * @return *this as bool
     */
    bool load(const std::set<std::string> &vap_indicators);

    /**
     * @brief helper: get exiting vap
     * @details any function that works on a specific vap does
     * the same: search the vap, set a variable when found
     * and set an error state when not found, this function
     * does it instead of copy/paste the same code
     * @param the calling function, string (to report error)
     * @param the requested vap, string
     * @return tuple<bool,std::vector<std::string>*> (found/not found, a
     * pointer to the vap's array)
     */
    std::tuple<bool, std::vector<std::string> *> get_vap(const std::string &calling_function,
                                                         const std::string &vap);

    /**
     * @brief helper: check if the line contains the key
     * @param line in the format key=value or commented ###key=value
     * @param the requested key, string
     * @return true/false if the line contains the key
     */
    bool is_key_in_line(const std::string &line, const std::string &key) const;

private:
    std::string m_configuration_file;

    // m_ok hoslds the internal state of the configuration
    // the user may query the success/fail state of the last command
    // simply by reading the value of this variable.
    // the access to it is via operator bool()
    // m_ok itslef may be changed because of a call to
    // may be changed because of const functions, therefore mutable
    mutable bool m_ok = false;

    // each string is a line in the original configuration file
    // that belongs to the "head" part. read the explenation at
    // the end of the cpp file for more details
    std::vector<std::string> m_hostapd_config_head;

    // a vector of pairs: <vap(the key),key/value pairs as a string>.
    // each key/value in .second of the pair is the line in the original
    // configuration file with the original key=value
    // e.g. the following lines in the configuration file:
    // bss=wlan0.1 <------------------------------  .first in the pair: "wlan0.1"
    // ctrl_interface=/var/run/hostapd <----------  each line is a string in .second vector
    // ap_isolate=1
    // ap_max_inactivity=60
    // bss_transition=1
    // interworking=1
    // disassoc_low_ack=1
    // bss=wlan0.2  <------------------------------ .first in the vector: "wlan0.2"
    // ctrl_interface=/var/run/hostXXd
    // ap_isolate=1
    // ap_max_inactivity=60
    // bss_transition=0
    // interworking=3
    // creates two entries in the map:
    // { "wlan0.1" : ["ctrl_interface=/var/run/hostapd", "ap_isolate=1", "ap_max_inactivity=60", "bss_transition=1", "interworking=1", "disassoc_low_ack=1"] },
    // { "wlan0.2" : ["ctrl_interface=/var/run/hosXXpd", "ap_isolate=1", "ap_max_inactivity=60", "bss_transition=0", "interworking=3"] }
    std::vector<std::pair<std::string, std::vector<std::string>>> m_hostapd_config_vaps;

    // see m_ok's comment
    mutable std::string m_last_message = "initial state, yet nothing was done";

    // for logs
    friend std::ostream &operator<<(std::ostream &, const Configuration &);
};

template <class func, class ap_predicate>
void Configuration::for_all_ap_vaps(func f, ap_predicate pred)
{
    auto f_with_iter = [&f](const std::string &vap, int iter) { f(vap); };

    int dummy(0);
    for_all_ap_vaps(f_with_iter, dummy, 10, pred);
}

template <class func, class iter, class ap_predicate>
void Configuration::for_all_ap_vaps(func f, iter current_iter, const iter end, ap_predicate pred)
{
    for_each(m_hostapd_config_vaps.begin(), m_hostapd_config_vaps.end(),
             [this, &f, &current_iter, &end,
              &pred](const std::pair<std::string, std::vector<std::string>> &vap) {
                 if (pred(vap.first)) {
                     if (end == current_iter) {
                         f(vap.first, end);
                     } else {
                         f(vap.first, current_iter++);
                     }
                 }
             });
}

// for logs
std::ostream &operator<<(std::ostream &, const Configuration &);

Configuration::Configuration(const std::string &file_name) : m_configuration_file(file_name) {}

Configuration::operator bool() const { return m_ok; }

bool Configuration::load(const std::set<std::string> &vap_indications)
{
    // please take a look at README.md (common/beerocks/hostapd/README.md) for
    // the expected format of hostapd configuration file.
    // loading the file relies on the expected format
    // otherwise the load fails

    // for cases when load is called more than once, we
    // first clear internal data
    m_hostapd_config_head.clear();
    m_hostapd_config_vaps.clear();

    // start reading
    std::ifstream ifs(m_configuration_file);
    std::string line;

    bool parsing_vaps = false;
    std::string cur_vap;

    // go over line by line in the file
    while (getline(ifs, line)) {

        // skip empty lines
        if (std::all_of(line.begin(), line.end(), isspace)) {
            continue;
        }

        // check if the string belongs to a vap config part and capture which one.
        auto end_comment = line.find_first_not_of('#');
        auto end_key     = line.find_first_of('=');

        std::string current_key(line, end_comment, end_key + 1);

        auto vap_iterator = vap_indications.find(current_key);
        if (vap_iterator != vap_indications.end()) {
            // from now on we are in the vaps area, all
            // key/value pairs belongs to vaps
            parsing_vaps = true;

            // copy the vap value
            cur_vap = std::string(line, end_key + 1);
            m_hostapd_config_vaps.push_back(std::make_pair(cur_vap, std::vector<std::string>()));
        }

        // if not a vap line store it in the header part of the config,
        // otherwise add to the currently being parsed vap storage.
        if (!parsing_vaps) {
            m_hostapd_config_head.push_back(line);
        } else {
            // we always adding to the last vap that was inserted
            m_hostapd_config_vaps.back().second.push_back(line);
        }
    }

    // if we've got to parsing vaps and no read errors, assume all is good
    m_ok = parsing_vaps && !ifs.bad();

    // return this as bool
    return *this;
}

bool Configuration::store()
{
    std::ofstream out_file(m_configuration_file, std::ofstream::out | std::ofstream::trunc);

    // store the head
    for (const auto &line : m_hostapd_config_head) {
        out_file << line << "\n";
    }

    // store the next ones ('bss=')
    for (auto &vap : m_hostapd_config_vaps) {
        // add empty line for readability
        out_file << "\n";
        for (auto &line : vap.second) {
            out_file << line << "\n";
        }
    }

    m_ok           = true;
    m_last_message = m_configuration_file + " was stored";

    // close the file
    out_file.close();
    if (out_file.fail()) {
        m_ok           = false;
    }

    return *this;
}

bool Configuration::set_create_head_value(const std::string &key, const std::string &value)
{
    // search for the key
    std::string key_eq(key + "=");
    auto line_iter = std::find_if(
        m_hostapd_config_head.begin(), m_hostapd_config_head.end(),
        [&key_eq, this](const std::string &line) -> bool { return is_key_in_line(line, key_eq); });

    // we first delete the key, and if the requested value is non empty
    // we push it to the end of the array

    // delete the key-value if found
    if (line_iter != m_hostapd_config_head.end()) {
        line_iter = m_hostapd_config_head.erase(line_iter);
    } else {
        m_last_message =
            std::string(__FUNCTION__) + " the key '" + key + "' for head was not found";
    }

    // when the new value is provided add the key back with that new value
    if (value.length() != 0) {
        m_hostapd_config_head.push_back(key_eq + value);
        m_last_message = std::string(__FUNCTION__) + " the key '" + key + "' was (re)added to head";
    } else {
        m_last_message = std::string(__FUNCTION__) + " the key '" + key + "' was deleted from head";
    }

    m_ok = true;
    return *this;
}

bool Configuration::set_create_head_value(const std::string &key, const int value)
{
    return set_create_head_value(key, std::to_string(value));
}

std::string Configuration::get_head_value(const std::string &key)
{
    std::string key_eq(key + "=");
    auto line_iter = std::find_if(
        m_hostapd_config_head.begin(), m_hostapd_config_head.end(),
        [&key_eq, this](const std::string &line) -> bool { return is_key_in_line(line, key_eq); });

    if (line_iter == m_hostapd_config_head.end()) {
        m_last_message = std::string(__FUNCTION__) + " couldn't find requested key in head: " + key;
        return "";
    }

    // return from just after the '=' sign to the end of the string
    return line_iter->substr(line_iter->find('=') + 1);
}

bool Configuration::set_create_vap_value(const std::string &vap, const std::string &key,
                                         const std::string &value)
{
    // search for the requested vap
    auto find_vap = get_vap(std::string(__FUNCTION__) + " key/value: " + key + '/' + value, vap);
    if (!std::get<0>(find_vap)) {
        return false;
    }
    auto existing_vap           = std::get<1>(find_vap);
    bool existing_vap_commented = existing_vap->front()[0] == '#';

    std::string key_eq(key + "=");
    auto line_iter = std::find_if(
        existing_vap->begin(), existing_vap->end(),
        [&key_eq, this](const std::string &line) -> bool { return is_key_in_line(line, key_eq); });

    // we first delete the key, and if the requested value is non empty
    // we push it to the end of the array

    // delete the key-value if found
    if (line_iter != existing_vap->end()) {
        line_iter = existing_vap->erase(line_iter);
    } else {
        m_last_message =
            std::string(__FUNCTION__) + " the key '" + key + "' for vap " + vap + " was not found";
    }

    // when the new value is provided add the key back with that new value
    if (value.length() != 0) {
        if (existing_vap_commented) {
            existing_vap->push_back('#' + key_eq + value);
        } else {
            existing_vap->push_back(key_eq + value);
        }
        m_last_message =
            std::string(__FUNCTION__) + " the key '" + key + "' for vap " + vap + " was (re)added";
    } else {
        m_last_message =
            std::string(__FUNCTION__) + " the key '" + key + "' for vap " + vap + " was deleted";
    }

    m_ok = true;
    return *this;
}

bool Configuration::set_create_vap_value(const std::string &vap, const std::string &key,
                                         const int value)
{
    return set_create_vap_value(vap, key, std::to_string(value));
}

std::string Configuration::get_vap_value(const std::string &vap, const std::string &key)
{
    // search for the requested vap
    auto find_vap = get_vap(std::string(__FUNCTION__), vap);
    if (!std::get<0>(find_vap)) {
        return "";
    }
    const auto &existing_vap = std::get<1>(find_vap);

    // from now on this function is ok with all situations
    // (e.g. not finding the requested key)
    m_ok = true;

    std::string key_eq(key + "=");
    auto line_iter = std::find_if(
        existing_vap->begin(), existing_vap->end(),
        [&key_eq, this](const std::string &line) -> bool { return is_key_in_line(line, key_eq); });

    if (line_iter == existing_vap->end()) {
        m_last_message = std::string(__FUNCTION__) +
                         " couldn't find requested key for vap: " + vap + "; requested key: " + key;
        return "";
    }

    // return from the just after the '=' sign to the end of the string
    return line_iter->substr(line_iter->find('=') + 1);
}

bool Configuration::disable_vap(const std::string &vap)
{
    if (!set_create_vap_value(vap, "start_disabled", "1")) {
        return false;
    }
    if (!set_create_vap_value(vap, "ssid", "")) {
        return false;
    }
    if (!set_create_vap_value(vap, "multi_ap", "0")) {
        return false;
    }
    if (!set_create_vap_value(vap, "multi_ap_backhaul_ssid", "")) {
        return false;
    }
    if (!set_create_vap_value(vap, "multi_ap_backhaul_wpa_passphrase", "")) {
        return false;
    }
    return true;
}

const std::string &Configuration::get_last_message() const { return m_last_message; }

std::tuple<bool, std::vector<std::string> *>
Configuration::get_vap(const std::string &calling_function, const std::string &vap)
{
    // search for the requested vap - ignore comments
    // by searching from the back of the saved vap (rfind)
    auto existing_vap =
        std::find_if(m_hostapd_config_vaps.begin(), m_hostapd_config_vaps.end(),
                     [&vap](const std::pair<std::string, std::vector<std::string>> &current_vap) {
                         return current_vap.first.rfind(vap) != std::string::npos;
                     });

    if (existing_vap == m_hostapd_config_vaps.end()) {
        m_last_message = calling_function + " couldn't find requested vap: " + vap;
        m_ok           = false;
        return std::make_tuple(false, nullptr);
    }

    m_ok = true;
    return std::make_tuple(true, &existing_vap->second);
}

bool Configuration::is_key_in_line(const std::string &line, const std::string &key) const
{
    // we need to make sure when searching for example
    // for "ssid", to ignore cases like:
    // multi_ap_backhaul_ssid="Multi-AP-24G-2"
    //                   ^^^^^
    // bssid=02:9A:96:FB:59:11
    //  ^^^^^
    // and we need to take into consideration
    // that the key might be or might not be commented.
    // so the search algorithm is:
    // - find the requested key and
    // - make sure it is either on the first position
    // or it has a comment sign just before it
    auto found_pos = line.rfind(key);
    bool ret = found_pos != std::string::npos && (found_pos == 0 || line.at(found_pos - 1) == '#');

    return ret;
}

std::ostream &operator<<(std::ostream &os, const Configuration &conf)
{
    os << "== configuration details ==\n"
       << "= ok:           " << conf.m_ok << '\n'
       << "= last message: " << conf.m_last_message << '\n'
       << "= file:         " << conf.m_configuration_file << '\n'
       << "= head:         " << '\n';

    for (const auto &line : conf.m_hostapd_config_head) {
        os << line << '\n';
    }

    os << "== vaps (total of: " << conf.m_hostapd_config_vaps.size() << " vaps) ==\n";

    for (const auto &vap : conf.m_hostapd_config_vaps) {
        os << "   vap: " << vap.first << "\n";
        for (const auto &line : vap.second) {
            os << line << '\n';
        }
    }

    return os;
}

static Configuration load_hostapd_config(const std::string &radio_iface_name)
{
    std::vector<std::string> hostapd_cfg_names = {
        "/tmp/run/hostapd-phy0.conf", "/tmp/run/hostapd-phy1.conf", "/var/run/hostapd-phy0.conf",
        "/home/vu/hostapd-phy1.conf", "/var/run/hostapd-phy2.conf", "/var/run/hostapd-phy3.conf",
        "/nvram/hostapd0.conf",       "/nvram/hostapd1.conf",       "/nvram/hostapd2.conf",
        "/nvram/hostapd3.conf",       "/nvram/hostapd4.conf",       "/nvram/hostapd5.conf",
        "/nvram/hostapd6.conf",       "/nvram/hostapd7.conf"};

    for (const auto &try_fname : hostapd_cfg_names) {

        Configuration hostapd_conf(try_fname);

        // try loading
        if (!hostapd_conf.load("interface=", "bss=")) {
            continue;
        }

        // check if it is the right one:
        // we are looking for the line in the vap that declares this vap: interface=radio_iface_name
        // we could equaly ask hostapd_conf if it has this vap, but there is no such interface
        // to do this. it should be something like: hostapd_conf.is_vap_exists(radio_iface_name);
        if (hostapd_conf.get_vap_value(radio_iface_name, "interface") != radio_iface_name &&
            hostapd_conf.get_vap_value(radio_iface_name, "bss") != radio_iface_name) {
            continue;
        }

        // all is good, return the conf
        return hostapd_conf;
    }

    // return an empty one since we couldn't find the
    return Configuration("file not found");
}

int update_credentials(char *ifname, char *ssid, char *key) {

    char path[255];
    struct wpa_ctrl *ctrl=NULL;
    sprintf(path, "/var/run/hostapd/%s", ifname);

    ctrl = wpa_ctrl_open(path, NULL);
    if (ctrl) {
        Configuration conf = load_hostapd_config(ifname);
        if (conf) {
        const std::string vap;
        if (ssid)
            conf.set_create_vap_value(vap, "ssid", ssid);
        if (key)
            conf.set_create_vap_value(vap, "wpa_passphrase", key);
            
        conf.set_create_vap_value(vap, "start_disabled", "");

            if (!conf.store()) {
            printf("Autoconfiguration: cannot save hostapd config!");
            return false;
            }
        }

        hostapd_cli_cmd_update(ctrl);
        wpa_ctrl_close(ctrl);
    }

    return true;
}
