/* Simple tool for update wifi credentials without restart network */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "hostapd.h"

void usage() {
	printf("wifi_cli -i {dev_name} -s {ssid} -k {key}");
}

int main(int argc, char **argv)
{
	if (argc < 3) {
		usage();
		return 0;
	}
	
	int ch;
	char *ifname = NULL, *ssid = NULL, *key = NULL;

	while ((ch = getopt(argc, argv, "i:s:k:")) != -1) {
		switch (ch) {
		case 'i':
			ifname = optarg;
			break;
		case 's':
			ssid = optarg;
			break;
		case 'k':
			key = optarg;
			break;
		default:
			usage();
			break;
		}
	}

	update_credentials(ifname, ssid, key);

	return 0;
}
