/*
 * test.h
 *
 *  Created on: Oct 4, 2022
 *      Author: vu
 */

#ifndef TEST_H_
#define TEST_H_

#ifdef __cplusplus
extern "C" {
#endif

int update_credentials(char *ifname, char *ssid, char *key);

#ifdef __cplusplus
}
#endif

#endif /* TEST_H_ */
