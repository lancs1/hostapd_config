CXXFLAGS+=-g -O -Wall -fPIC
LDFLAGS = -shared   # linking flags
RM = rm -f   # rm command
TARGET_LIB = libconf_hostapd.so  # target lib
BIN = wifi_cli
LIBS = -lconf_hostapd

SRCS = test.cpp  # source files

.PHONY: all
all: ${TARGET_LIB} ${BIN}

$(TARGET_LIB): hostapd.o os_unix.o
	$(CXX) ${LDFLAGS} -o $@ $^

$(BIN): main.o
	$(CC) -o $@ $^ ${LIBS}
	
%.o:%.cpp
	$(CXX)  -c $(CXXFLAGS) $< -o $@

%.o:%.c
	$(CC)  -c $(CXXFLAGS) $< -o $@
	
.PHONY: clean
clean:
	- rm *.o ${TARGET_LIB} ${BIN}
